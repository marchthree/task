package com.task.test;

import java.text.ParseException;
import java.util.Date;

import org.quartz.CronExpression;

import com.system.comm.utils.FrameTimeUtil;

public class CronTest {

	public static void main(String[] args) throws ParseException {
		Date curDate = new Date();
		CronExpression cronExpression = new CronExpression("0/5 * * * * ?");
		System.out.println("当前时间: " + FrameTimeUtil.parseString(curDate));
		Date nextDate = cronExpression.getNextValidTimeAfter(curDate);
        System.out.println("下次执行: " + FrameTimeUtil.parseString(nextDate));
	}
}
